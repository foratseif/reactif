import time
import threading

import reactif


class App(reactif.Elemf):

    def __init__(self):
        pass

    def layout(self):
        return ('p', "hello world!")


class Handler():

    def __init__(self):
        self.connections = {}

    async def ws_open(self, ws):
        print("ws open")
        return

    async def ws_msg(self, ws, msg):
        print("ws msg", msg)
        return

    async def ws_close(self, ws):
        print("ws close")
        return


if __name__ == '__main__':
    host = "0.0.0.0"
    port = 8080

    server = reactif.Server(
        host=host,
        port=port,
        handler=Handler(),
    )

    server.run()


