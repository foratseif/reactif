from lxml import etree

import lib

if __name__ == '__main__':

    tup = ('html',
        ('head',
            ('title', "App"),
            #{'meta', {'charset': "utf-8"}, },
            #{'meta', {'http-equiv': "Content-type", 'content': "text/html; charset=utf-8"}, },
            #{'meta', dict(name="viewport", content="width=device-width, initial-scale=1"), }
        ),
        ('body',
            ('div',
                ('h1', "App"),
                ('p', "gogi"),
            ),
        ),
    )


    fridge = lib.to_lxml_element(tup)
    tree = etree.ElementTree(fridge)

    #tree.docinfo.doctype='html'

    #print(tree.docinfo.doctype)
    print(etree.tostring(tree))

