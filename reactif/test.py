import lib

if __name__ == '__main__':

    tup = ("div", {'class': "i will go"},
        ("p", {"class": "i will change"}, "asf", "asf", "asf",),
        ("li", "Header!"),
        ("li",
            "Header!",
            ("div",
                "this is a div",
                ("div",
                    ("div", "this is a div"),
                ),
            ),
        ),
        ("li", "Header!"),
        ("li", "Header!"),
        ("li", "Header!"),
        ("li", "Header!"),
        ("li", "Header!"),
        ("li", "Header!"),
    )

    tup2 = ("div",
        ("p", {"class": "i changed"}, "asf", "asf", "asf",),
        ("li", "Header!"),
        ("li",
            "Header!",
            ("div",
                "this is a div",
                ("div",
                    ("div", "this is a div aaa"),
                ),
            ),
        ),
        ("li", "Header!"),
        ("li", "Header!"),
        ("li", "Header!"),
        ("li", "Header!"),
        ("li", "Header!"),
        ("li", "Header!"),
    )

    tree1 = lib.to_lxml_element(tup)
    tree2 = lib.to_lxml_element(tup2)

    for command in lib.diff(tree1, tree2):
        print(command)


