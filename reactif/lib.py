from lxml import etree
from xmldiff import main

def to_lxml_element(defn):
    # init vars
    tag     = defn[0]
    attrs   = dict()
    content = list()

    for d in defn[1:]:
        if type(d) is dict:
            attrs.update(d)
        else:
            content.append(d)

    # create element
    elem = etree.Element(tag, **attrs)

    # avoid <thing/> syntax
    if type(defn) is tuple:
        elem.text = ""

    # init content

    last_child = None
    for i in content:

        # if Element
        if type(i) is etree.Element:
            last_child = i
            elem.append(last_child)

        # else if tuple or set
        elif type(i) in [tuple, set]:
            last_child = to_lxml_element(i)
            elem.append(last_child)

        # else if string and last_child exists
        elif last_child is not None:
            last_child.tail = str(i)
            last_child = None

        # else if last_child does not exists and there is already text
        elif len(elem.text):
            br = etree.SubElement(elem, "br")
            br.tail = str(i)

        # else: add text to start of element
        else:
            elem.text = str(i)

    return elem

def diff(t1, t2):
    return main.diff_trees(t1, t2)
