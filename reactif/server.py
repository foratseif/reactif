import asyncio
import aiohttp
from aiohttp import web

class Server:

    def __init__(self, host='0.0.0.0', port=8080, handler=None):
        self.loop = None
        self.host = host
        self.port = port

        if handler is None:
            assert handler is not None

        self.handler = handler

    async def init_html(self, request):
        doc = ""
        with open("index.html", "r") as f:
            doc += f.read()

        return web.Response(
            content_type='text/html',
            text=doc,
        )

    async def websocket_handler(self, request):
        ws = web.WebSocketResponse()
        await ws.prepare(request)

        await self.handler.ws_open(ws)

        async for msg in ws:
            if msg.type == aiohttp.WSMsgType.TEXT:
                await self.handler.ws_msg(ws, msg.json())
            elif msg.type == aiohttp.WSMsgType.ERROR:
                print(ws.exception())

        await self.handler.ws_close(ws)

    def trigger_some_shit(self, shit):
        if self.loop is not None:
            self.loop.call_soon_threadsafe(print, "fuck::::", shit)

    def aiohttp_server(self):
        app = web.Application()
        app.add_routes([
            web.get('/', self.init_html),
            web.get('/ws', self.websocket_handler),
        ])
        runner = web.AppRunner(app)
        return runner

    def start_running(self):
        runner = self.aiohttp_server()
        self.loop = asyncio.new_event_loop()
        asyncio.set_event_loop(self.loop)
        self.loop.run_until_complete(runner.setup())
        site = web.TCPSite(runner, self.host, self.port)
        self.loop.run_until_complete(site.start())

    def continue_running(self):
        self.loop.run_forever()

    def run(self):
        self.start_running()
        self.continue_running()
