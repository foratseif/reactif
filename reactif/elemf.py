class Elemf:

    @classmethod
    def render(cls, ch):
        if isinstance(ch, cls):
            return ch._render()
        else:
            return ch

    def layout(self):
        raise NotImplementedError("Subclasses should implement this!")

    def _render(self):
        layout = self.layout()

        if type(layout) is tuple:
            layout = tuple(self.render(el) for el in layout)

        return layout


